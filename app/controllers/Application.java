package controllers;

import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class Application extends Controller {

    @Inject
    public Application() {

    }

    private final Form<User> userForm = Form.form(User.class);

    public Result index() {
	// Wenn der User eingeloggt ist dann login sonst vorschlag
	return redirect(routes.Application.login());
    }

    public Result login() {
	return ok(views.html.login.render(userForm));
    }

    public Result register() {
	return ok(views.html.register.render(userForm));
    }

    public Result submitUser() {
	Form<User> submittedForm = userForm.bindFromRequest();
	if (submittedForm.hasErrors()) {
	    return ok(views.html.login.render(submittedForm));
	} else {
	    User user = submittedForm.get();
	    user.save();
	    flash("message", "Thanks for your registration!");
	    return redirect(routes.Application.index());
	}
    }

}
