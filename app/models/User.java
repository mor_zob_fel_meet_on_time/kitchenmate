package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class User extends Model {

    @Id
    public Long id;

    @Required
    public String vorname;

    @Required
    public String nachname;

    @Required
    @Email
    public String email;
    
    @Required
    public String password;

}
