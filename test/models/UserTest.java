package models;

import static helpers.TestSetup.testGlobalSettings;
import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

import org.junit.Test;

import com.avaje.ebean.Ebean;

public class UserTest {

    @Test
    public void whenVornameIsEmptyThenGivAValidationError() throws Exception {
	running(fakeApplication(testGlobalSettings()), new Runnable() {

	    @Override
	    public void run() {
		// TODO Auto-generated method stub
		User user = sampleUser();
		user.save();
		assertThat(rowCount()).isEqualTo(0);
	    }

	    private int rowCount() {
		return Ebean.find(User.class).findRowCount();
	    }

	});

    }

    private User sampleUser() {
	User user = new User();
	user.vorname = "";
	return user;
    }

}
