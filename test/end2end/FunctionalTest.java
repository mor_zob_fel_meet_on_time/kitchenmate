package end2end;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import java.util.HashMap;

import org.junit.Test;

import play.libs.F.Callback;
import play.test.TestBrowser;
import controllers.routes;

public class FunctionalTest {

    @Test
    public void whenBrowseToHomeSiteAndClickOnRegistrationThenRegistrationThenLoginThenBrowsToVorschlag()
	    throws Exception {
	HashMap<String, Object> dbSettings = new HashMap<String, Object>();
	dbSettings.put("db.default.url",
		"jdbc:mysql//localhost:3306/kitchenmatetest");

	running(testServer(3333, fakeApplication()), HTMLUNIT,
		new Callback<TestBrowser>() {

		    @Override
		    public void invoke(TestBrowser browser) throws Throwable {
			browser.goTo("http://localhost:3333"
				+ routes.Application.index().url());
			assertThat(browser.title()).isEqualTo("Kitchen Mate");
			assertThat(browser.pageSource()).contains(
				"Registrieren");
			browser.$("#registrieren").click();
			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/register");
			assertThat(browser.pageSource()).contains("Vorname");
			assertThat(browser.pageSource()).contains("Name");
			assertThat(browser.pageSource()).contains("Email");
			assertThat(browser.pageSource()).contains("password");

			browser.fill("#val_vorname").with("John");
			browser.fill("#val_nachname").with("Doe");
			browser.fill("#val_email").with("john.doe@doe.com");
			browser.fill("#val_password").with("secret");

			browser.$("#registrieren").click();

			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/login");

			browser.fill("#val_email").with("john.doe@doe.com");
			browser.fill("#val_password").with("secret");
			browser.$("#login").click();

			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/vorschlag");

			assertThat(browser.pageSource()).contains("John");
			assertThat(browser.pageSource()).contains("Doe");
			assertThat(browser.pageSource()).contains(
				"john.doe@doe.com");

		    }
		});

    }
}
